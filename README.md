# POC Leaflet + Canvas Markers

Teste de desempenho exibindo e manipulando grande quantidade de pontos no mapa.

**Funcionalidades**

- [x] - pontos com drag-drop
- [x] - polyline com hover e click
- [ ] - async data loading

**Bugs**

- [x] - quando altera o zoom, os marcadores voltam para a posição inicial porque os valores de lat e lng não estão sendo atualizados no state.
- [x] - Ao arrastar e soltar marcadores, às vezes ele não fica na posição correta.
- [x] - Depois de soltar um item, ao clicar novamente, pode "gerar" um novo marcador.

**TODO**

- Exibir rotas com hover, click e "pernoite(?)"

## Setup

```bash
yarn
```

## Run

```bash
yarn start
```
