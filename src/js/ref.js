export const refs = {};

export function setRef(name, value) {
  refs[name] = value;
  return refs[name];
}
