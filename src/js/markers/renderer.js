import { refs } from '../ref'
import { state } from '../state'

export function renderMarkers() {
  const context = refs.markersLayer.canvas().getContext('2d')
  context.clearRect(0, 0, refs.markersLayer.canvas().width, refs.markersLayer.canvas().height)

  Object.keys(state.activeMarkers).forEach((id) => {
    if (id === state.dragging || id === state.hovering) {
      return
    }
    const { x, y } = state.activeMarkers[id]

    renderMarker(x, y, refs.markersLayer)
  })
}

export function renderDraggingMarker() {
  const context = refs.activeMarkerLayer.canvas().getContext('2d')
  context.clearRect(
    0,
    0,
    refs.activeMarkerLayer.canvas().width,
    refs.activeMarkerLayer.canvas().height
  )

  if (!state.dragging) return
  const { x, y } = state.activeMarkers[state.dragging]
  renderMarker(x, y, refs.activeMarkerLayer)
}

// export function renderHoverMarker() {}

export function renderMarker(x, y, layer) {
  const context = layer.canvas().getContext('2d')
  context.drawImage(refs.markerShadow, x - state.markerWidth / 2, y - state.markerHeight)
  context.drawImage(refs.markerIcon, x - state.markerWidth / 2, y - state.markerHeight)
}
