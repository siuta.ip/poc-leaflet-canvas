import { setState, state } from '../state'
import { setDragging, saveMarkerPosition } from '../actions'
import { renderMarkers, renderDraggingMarker } from './renderer'
import { refs } from '../ref'

/**
 * Return id of marker that position match with x and y position
 *
 * in case of multiple markers near by position, return marker
 * with highest order value
 *
 * @param {number} x
 * @param {number} y
 * @returns string | null
 */
function findMarker(x, y) {
  return (
    markersInPoint(x, y)
      .map((id) => ({ id, ...state.activeMarkers[id] }))
      // @todo: verify ordering
      .sort((a, b) => {
        if (a.order > b.order) {
          return -1
        }
        if (a.order < b.order) {
          return -1
        }

        return 0
      })[0]?.id || null
  )
}

function markersInPoint(x, y) {
  return Object.keys(state.activeMarkers).reduce((markers, id) => {
    const marker = state.activeMarkers[id]

    const markerStart = {
      x: marker.x - state.markerWidth / 2,
      y: marker.y - state.markerHeight,
    }
    const markerEnd = {
      x: marker.x + state.markerWidth / 2,
      y: marker.y,
    }

    if (x >= markerStart.x && x <= markerEnd.x) {
      if (y >= markerStart.y && y <= markerEnd.y) {
        return [...markers, id]
      }
    }

    return markers
  }, [])
}
export function handleDragStart({ containerPoint: { x, y } }) {
  setDragging(findMarker(x, y))
  renderMarkers()
  renderDraggingMarker()
}

export function handleDragMove({ containerPoint: { x, y } }) {
  if (!state.dragging) return

  setState({
    activeMarkers: {
      ...state.activeMarkers,
      [state.dragging]: {
        ...state.activeMarkers[state.dragging],
        x,
        y,
      },
    },
  })
  renderDraggingMarker()
}

export function handleDragEnd({ containerPoint }) {
  if (!state.dragging) return

  const { lat, lng } = refs.map.containerPointToLatLng(containerPoint)
  saveMarkerPosition({ lat, lng })

  setDragging(false)
  renderMarkers()
  renderDraggingMarker()
}
