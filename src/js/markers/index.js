import { refs } from '../ref'
import { setState, state } from '../state'
import * as events from './events'
import * as renderer from './renderer'

export function setup(markers = {}) {
  setState({ ...state, markers })
  setupViewport()

  refs.map.on('mousedown', events.handleDragStart)
  refs.map.on('mouseup', events.handleDragEnd)
  refs.map.on('mousemove', events.handleDragMove)
}

export function update() {
  setupViewport()
}

export function render() {
  renderer.renderMarkers()
  renderer.renderDraggingMarker()
}

function setupViewport() {
  const activeMarkers = {}

  Object.keys(state.markers).forEach((id) => {
    const { lat, lng } = state.markers[id]
    if (refs.map.getBounds().contains({ lat, lng })) {
      const pos = refs.map.latLngToContainerPoint({ lat, lng })
      activeMarkers[id] = { lat, lng, x: pos.x, y: pos.y }
    }
  })
  setState({ activeMarkers })
}
