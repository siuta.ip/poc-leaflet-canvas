import { refs } from '../ref'
import { setState, state } from '../state'
import { renderLines, renderActiveLine } from './renderer'

export function setup(routes) {
  setState({ routes })

  refs.map.on('mousemove', function ({ containerPoint: { x, y } }) {
    if (state.moving) return
    renderActiveLine()

    if (state.dragging) return
    setState({ hoveredRoute: null })

    Object.keys(refs.drawnLines)
      .filter((id) => {
        const { bounds } = refs.drawnLines[id]
        if (x >= bounds.xMin && x <= bounds.xMax) {
          if (y >= bounds.yMin && y <= bounds.yMax) {
            return true
          }
        }
        return false
      })
      .forEach((id) => {
        const { path } = refs.drawnLines[id]
        if (refs.linesLayer.canvas().getContext('2d').isPointInStroke(path, x, y)) {
          setState({ hoveredRoute: id })
        }
      })
    renderActiveLine()
  })
}

export function render() {
  renderLines()
  renderActiveLine()
}
