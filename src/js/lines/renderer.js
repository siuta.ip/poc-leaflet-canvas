import { refs, setRef } from '../ref'
import { state } from '../state'

export function renderLines() {
  const canvas = refs.linesLayer.canvas()
  const context = canvas.getContext('2d')
  context.clearRect(0, 0, canvas.width, canvas.height)

  setRef('drawnLines', {})

  context.clearRect(0, 0, canvas.width, canvas.height)
  Object.keys(state.routes).forEach((id) => {
    const route = state.routes[id]
    drawLine({ ...route, id }, canvas, 'white')
  })
}

export function renderActiveLine() {
  const canvas = refs.activeLineLayer.canvas()
  canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)

  if (!state.hoveredRoute) return

  const line = state.routes[state.hoveredRoute]

  if (!line) return

  drawLine({ ...line, id: state.hoveredRoute }, canvas, 'red')
}

export function drawLine({ points, id }, canvas, color) {
  const context = canvas.getContext('2d')

  if (state.dragging) return

  const path = new Path2D()
  context.strokeStyle = color
  context.lineWidth = 4

  const { bounds, updateBounds } = createBounds()

  points.forEach(({ lat, lng }, index) => {
    const { x, y } = refs.map.latLngToContainerPoint({ lat, lng })

    if (index === 0) {
      updateBounds(x, y, true)
      path.moveTo(x, y)
      return
    }
    updateBounds(x, y)

    path.lineTo(x, y)
  })

  refs.drawnLines[id] = { bounds, path }
  context.stroke(path)
}

function createBounds(x = 0, y = 0) {
  const bounds = {
    xMin: x,
    xMax: x,
    yMax: y,
    yMin: y,
  }

  const updateBounds = (x, y, reset = false) => {
    if (reset) {
      bounds.xMin = x
      bounds.xMax = x
      bounds.yMax = y
      bounds.yMin = y
    }
    if (x < bounds.xMin) {
      bounds.xMin = x
    }
    if (x > bounds.xMax) {
      bounds.xMax = x
    }

    if (y < bounds.yMin) {
      bounds.yMin = y
    }
    if (y > bounds.yMax) {
      bounds.yMax = y
    }

    return { bounds, updateBounds }
  }

  return { bounds, updateBounds }
}
