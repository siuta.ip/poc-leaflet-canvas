/**
 * Types definition
 *
 * type Marker = {
 *   order: number,
 *
 *   // geolocation
 *   lat: number,
 *   lng: number,
 *
 *   // position in canvas
 *   x: number,
 *   y: number,
 * }
 *
 * type MarkersList = {
 *   // key is id of marker
 *   [key: string]: Maker
 * }
 */

export const state = {
  // styles properties of markers
  markerWidth: 25,
  markerHeight: 41,

  /**
   * list with all markers
   *
   * @type MarkersList
   */
  markers: {},

  /**
   * markers in viewport
   *
   * @type MakersList
   */
  activeMarkers: {},

  /**
   * id of clicked marker
   *
   * @type string
   */
  dragging: null,

  /**
   * id of hovered marker
   *
   * @type string
   */
  hovering: null,
}

export function setState(newState) {
  Object.keys(newState).forEach((key) => {
    state[key] = newState[key]
  })
}
