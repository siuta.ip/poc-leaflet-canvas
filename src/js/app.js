import { refs, setRef } from './ref'
import { fetchPoints, fetchRoutes } from './data'
import * as L from './leaflet'
import * as markers from './markers'
import * as lines from './lines'

export function setup(id = 'root') {
  preloadImages()
  setupMap(id)

  const layers = ['lines', 'activeLine', 'markers', 'activeMarker']

  layers.forEach((name) => createLayer(name))

  render()
}

function render() {
  markers.render()
  lines.render()
}

function update() {
  markers.update()
}

let imagesLoaded = 0

const imageLoad = () => {
  imagesLoaded += 1
  if (imagesLoaded >= 2) render()
}

function preloadImages() {
  setRef('markerIcon', new Image())
  refs.markerIcon.src = '/marker-icon.png'
  refs.markerIcon.onload = imageLoad

  setRef('markerShadow', new Image())
  refs.markerShadow.src = '/marker-shadow.png'
  refs.markerShadow.onload = imageLoad
}

function createLayer(name) {
  const layer = L.canvasOverlay()
  layer.addTo(refs.map)
  setRef(`${name}Layer`, layer)
}

function setupMap(id) {
  setRef('map', L.map(id))

  refs.map.setView([-20.413442150600797, -115.15749652566164], 4)

  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(refs.map)

  markers.setup(fetchPoints(3000))
  lines.setup(fetchRoutes())

  refs.map.on('moveend', () => {
    update()
    render()
  })
}
