import { v4 as uuid } from 'uuid'

/**
 * generate sample markers data
 *
 * @param {number} points quantity of markers to display in the map
 */
export function fetchPoints(points) {
  const columns = 100
  const lines = points / columns
  let line = 0
  const markers = {}

  while (line < lines) {
    let column = 0

    while (column < columns) {
      const id = uuid()
      let lat = -6.413442150600797 - column * 0.5
      let lng = -65.15749652566164 - line * 0.5

      markers[id] = {
        order: line * 1000 + column * 10,
        lat,
        lng,
        x: null,
        y: null,
      }
      column = column + 1
    }

    line = line + 1
  }
  return markers
}

export function fetchRoutes(numberOfRoutes = 100, rangeOfPointsInRoute = [500, 1000]) {
  const routes = {}
  for (let routeIndex = 0; routeIndex < numberOfRoutes; routeIndex++) {
    const { id, ...route } = generateRoute(rangeOfPointsInRoute)
    routes[id] = route
  }
  return routes
}

function random(min, max) {
  return Math.random() * (max - min + 1) + min
}

// lat: 21.69826549685252, lng: -33.92578125000001
// lat: -53.69670647530323, lng: -196.34765625000003
function generateRoute([minPoints, maxPoints]) {
  const id = uuid()
  const points = []
  const numberOfPoints = ~~random(minPoints, maxPoints)

  const latMin = -53.69670647530323
  const latMax = 21.69826549685252

  const lngMin = -196.34765625000003
  const lngMax = -33.92578125000001

  for (let pointIndex = 0; pointIndex < numberOfPoints; pointIndex++) {
    const refLat = points[pointIndex - 1]?.lat || random(latMin, latMax)
    const refLng = points[pointIndex - 1]?.lng || random(lngMin, lngMax)
    const diff = 1
    const xDirection = random(0, 10) > 5 ? -1 : 1
    const yDirection = random(0, 10) > 5 ? -1 : 1
    const moveLat = xDirection * diff
    const moveLng = yDirection * diff
    const lat = refLat + moveLat
    const lng = refLng + moveLng
    points.push({ lat, lng })
  }

  return {
    id,
    points,
  }
}
