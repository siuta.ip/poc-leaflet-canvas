import { refs } from './ref'
import { state, setState } from './state'

/**
 * @param {string | null | false } dragging  Marker.id or false to stop dragging
 */
export function setDragging(dragging = null) {
  if (!dragging) {
    refs.map.dragging.enable()
  } else {
    refs.map.dragging.disable()
  }

  setState({
    dragging,
  })
}

export function saveMarkerPosition({ lat, lng }) {
  if (!state.dragging) return

  setState({
    ...state,
    markers: {
      ...state.markers,
      [state.dragging]: {
        ...state.markers[state.dragging],
        lat,
        lng,
      },
    },
    activeMarkers: {
      ...state.activeMarkers,
      [state.dragging]: {
        ...state.activeMarkers[state.dragging],
        lat,
        lng,
      },
    },
  })
}
